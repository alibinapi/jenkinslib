UTILISATION DES LIBRAIRIES DANS JENKINS
But : factoriser, mutualiser pour rendre plus lisible son jenkinsfile quand on ades répétitions de pipelines en pipelines

Etapes :
- Créer trois repertoires:
  mkdir src ressources vars 
  On n'utilisera dans cet exemple que le repertoire vars

- Créer le fichier helloworld.groovy dans le repertoire vars:
  vim vars/helloworld.groovy

- Le contenu de helloworld.groovy est le suivant :
  #!/usr/bin/env groovy
  import groovy.json.*
  def call(body){
   println("Hello World !!!")
  }

- On crée un dépot sur gitlab ou sur github et on fait un push

- On va sur Administrer Jenkins / configurer le système
  et dans la rubrique Global Pipeline Libraries, on fait un ajout de librairie en cliquant sur le bouton Ajouter

- On doit avoir la configuration suivante :
     - Librarie Name : jenkinsLib (par exemple)
     - Default version : master (le nom de la branche concernée)
     - Les options à prendre en compte : 
         - Load implicity
         - Allow default version to be overriden
         - Include @ Librarie in job recent changes 
     - Retrieve method : 
        - Modern SCM (à prendre en compte pour spécifier qu'on utilise git)
     - Source Code Management :
        - Git (à prendre en compte)
             - Projet Repository : https://gitlab.com/alibinapi/jenkinsLib.git (par exemple; on peut le faire aussi avec les clés ssh) 
             - On pourra ajouter des credentials si c'est un projet privé

- On crée un pipeline. Au lancement du build, on appellera la fonction helloworld
Le contenu du script du pipeline est le suivant :
helloworld{} (appel de la librairie sans passage de variable)

A la sortie de la console, on voit bien que hello world !! est bien affiché


